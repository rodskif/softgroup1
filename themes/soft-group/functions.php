<?php
/**
 * Підключаємо локалізацію шаблону
 */
load_theme_textdomain('sg', get_template_directory() . '/languages');


/**
 * Додаємо favico, charset, viewport
 */
function add_head_meta_tags()
{
    ?>
    <link rel="shortcut icon" href="<?php bloginfo("template_url"); ?>/favico.png" type="image/x-icon">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php
}
add_action('wp_head', 'add_head_meta_tags');


/**
 * Реєструємо місце для меню
 */

register_nav_menus( array(
    'primary' => __('Primary', 'sg'),
) );


/**
 * Реєструємо сайдбари теми
 */
if ( function_exists('register_sidebar') )
{
    register_sidebars(1, array(
        'id' => 'left',
        'name' => __('Left sidebar', 'sg'),
        'description' => '',
        'before_widget' => '<div class="well">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>'
    ));

    register_sidebars(1, array(
        'id'            => 'copyright',
        'name'          => __('Copyright', 'sg'),
        'description'   => '',
        'before_widget' => '<div class="widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<p class="widget-title">',
        'after_title'   => '</p>'
    ));
}

/**
 * Підключаємо підтримку мініатюр
 */
if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 150, 150 );
}
/**
 * Можемо добавити різні розміри для картинок
 */
if ( function_exists( 'add_image_size' ) ) { 
    //add_image_size( 'photo', 148, 148, true );
}


/**
 * Реєструємо формати постів
 */
function add_post_formats(){
    add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' ) );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'custom-background' );
    add_theme_support( 'custom-header' );
    add_theme_support( 'custom-logo' );
}
add_action( 'after_setup_theme', 'add_post_formats', 11 );


/**
 * Замінюємо стандартне закінчення обрізаного тексту з [...] на ...
 */
function custom_excerpt_more( $more ) {
	return ' ...';
}
add_filter( 'excerpt_more', 'custom_excerpt_more' );


/**
 * Замінюємо стандартну довжину обрізаного тексту
 */
function custom_excerpt_length( $length ) {
	return 200;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

/**
 * Підключаємо javascript файли
 */
function add_theme_scripts(){
    wp_enqueue_script("jquery");
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery') );
    wp_enqueue_script( 'init', get_template_directory_uri() . '/js/init.js', array('jquery', 'bootstrap') );
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );

class gorgeous_taxonomy extends WP_Widget
{
    public function __construct() {
        parent::__construct("gorgeous_widget", "Gorgeous taxonomy Widget",
            array("description" => "A gorgeous widget to show different taxonomy"));
    }

    public function form($instance) {
        $title = "";
        $select = "";
        // если instance не пустой, достанем значения
        if (!empty($instance)) {
            $title = $instance["title"];
            $select = $instance["select"];
        }

        $tableId = $this->get_field_id("title");
        $tableName = $this->get_field_name("title");
        echo '<label for="' . $tableId . '">Title</label><br>';
        echo '<input id="' . $tableId . '" type="text" name="' .
            $tableName . '" value="' . $title . '"><br>';

//        $tax = get_terms('category', 'orderby=count&hide_empty=0');
        $taxId = $this->get_field_id("select");
        $tax = get_taxonomies($labels);

        echo '<label for="' .  $taxId . '">Taxonomy</label><br>';
        echo '<select>';
        foreach( $tax as $taxonomy ) {
            echo '<option>'. $taxonomy. '</option>';
//            echo '<p>'. $taxonomy. '</p>';
        }
        echo '</select>';

        $taxNum =

//        echo '<select>';
//            echo '<option>'.$tax['category'].'</option>';
//        echo '</select>';
//        $textId = $this->get_field_id("text");
//        $textName = $this->get_field_name("text");
//        echo '<label for="' . $textId . '">Text</label><br>';
//        echo '<textarea id="' . $textId . '" name="' . $textName .
//            '">' . $text . '</textarea>';
    }

    public function widget($args, $instance) {
        $title = $instance["title"];
        $text = $instance["text"];

        echo "<h2>$title</h2>";
//        echo "<p>$text</p>";
    }
}

add_action("widgets_init", function () {
    register_widget("gorgeous_taxonomy");
});


/**
 * Підключаємо css файли
 */
function add_theme_style(){
    //wp_enqueue_style( 'fonts', get_template_directory_uri() . '/fonts/stylesheet.css');
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_enqueue_style( 'blog-home', get_template_directory_uri() . '/css/blog-home.css');
    wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css');
}
add_action( 'wp_enqueue_scripts', 'add_theme_style' );

add_action('test', 'test');
function test( $a ){
    return '1';
}

add_filter('test', 'test2');
function test2( $a ){
    return '2';
}

?>