<div>
<form class="form" role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div class="input-group">
    <input class="form-control" type="text" value="" name="s" id="s" />
    <span class="input-group-btn">
        <button class="btn btn-default" type="submit" name="submit" id="searchsubmit" value="">
            <span class="glyphicon glyphicon-search"></span>
        </button>
    </span>
    </div>
</form>
</div>