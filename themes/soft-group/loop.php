<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>    
        <h2><a href="<?php the_permalink()?>"><?php the_title(); ?></a></h2>
        <p class="lead">
            <?= __('by', 'sg') ?> <a href="#"><?php the_author();  ?></a>
        </p>
        <p><span class="glyphicon glyphicon-time"></span> <?= __('Posted on', 'sg') ?> <?php the_date(); ?> <?= __('at', 'sg') ?> <?php the_time(); ?></p>
        <hr>
        <?php the_post_thumbnail(array(900, 300), array('class' => 'img-responsive'), 'post-thumbnails'); ?>
        <hr>
        <?php the_excerpt(); ?>
        <a class="btn btn-primary" href="<?php the_permalink(); ?>"><?= __('Read More', 'sg') ?> <span class="glyphicon glyphicon-chevron-right"></span></a>
        <hr>
<!--        the_permalink();-->
<!---->
<!--        the_excerpt();-->
<!---->
<!--        the_post_thumbnail();-->
<!---->
<!--        the_time();-->

    <?php endwhile; ?>
<?php endif; ?>