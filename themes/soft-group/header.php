<!DOCTYPE>
<html>
  <head>
        <title>
            <?php
            global $page, $paged;
            wp_title('|', true, 'right');
            bloginfo('name');
            $site_description = get_bloginfo('description', 'display');
            if ($site_description && (is_home() || is_front_page()))
                echo " | $site_description";
            if ($paged >= 2 || $page >= 2 )
                echo ' | '.sprintf(__('Page %s', 'striped'), max($paged, $page));
            ?>
        </title>
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
    <header id="header">

        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Start Bootstrap</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
<!--                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">-->
                    <?php wp_nav_menu( array(
                        'menu'              => 'Навигация',
                        'container'         => 'div',
                        'container_class'   => 'collapse navbar-collapse',
                        'container_id'      => 'bs-example-navbar-collapse-1',
                        'menu_class'        => 'nav navbar-nav',
                    )); ?>
<!--                    <ul class="nav navbar-nav">-->
<!--                        <li>-->
<!--                            <a href="#">About</a>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <a href="#">Services</a>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <a href="#">Contact</a>-->
<!--                        </li>-->
<!--                    </ul>-->
<!--                </div>-->
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </nav>

    </header>